import be_client
import tag_coding


class ProcessedGroup:
    def __init__(self, group):
        self.group = group
        self.id = group["id"]
        self.automatic_tag = tag_coding.decode_tag(group["description"])


def preload_groups(groups):
    groups_preprocess = []
    for group in groups:
        g = ProcessedGroup(group)
        if g.automatic_tag is not None:
            groups_preprocess.append(g)

    return groups_preprocess


def process_one_host(host, groups):
    if "manufacturer" not in host or ("model" not in host and "product" not in host):
        return

    manufacturer = host["manufacturer"]
    if "product" in host:
        product = host["product"]
    else:
        product = host["model"]

    found_group = None

    for group in groups:
        if "product" in group.automatic_tag and group.automatic_tag["product"] == product and \
                "manufacturer" in group.automatic_tag and group.automatic_tag["manufacturer"] == manufacturer:
            found_group = group
            break

    if found_group is None:
        found_group = be_client.make_group(manufacturer + " " + product, tag_coding.encode_as_tag(manufacturer, product))
        if 'id' not in found_group:
            print(f"[-] Skipping device {host['id']}, a non-automatic group for {manufacturer} {product} already exists")
            return
        found_group = ProcessedGroup(found_group)
        # We also append in into array of groups.
        groups.append(found_group)

    be_client.add_host_to_group(found_group.id, host["id"])
    return


def sync_state():
    groups = preload_groups(be_client.get_groups())
    page = 1
    hosts = be_client.get_hosts(page)
    while len(hosts['data']) != 0:
        for host in hosts['data']:
            process_one_host(host, groups)

        page += 1
        hosts = be_client.get_hosts(page)


if __name__ == "__main__":
    sync_state()
