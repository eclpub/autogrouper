import json

delimiter = "\xc2\xa0"


def encode_as_tag(manufacturer, product):
    return "Automatic: " + manufacturer + delimiter + product


def decode_tag(tag):
    if tag.startswith("Automatic: "):
        non_prefixed = tag[11:]
        items = non_prefixed.split(delimiter)
        if len(items) < 2:
            return None
        manufacturer = items[0]
        product = delimiter.join(items[1:])
        return {"manufacturer": manufacturer, "product": product}
    else:
        return None
