# This source image is a bit bloated, couold take it down a notch with a custom rebuild
FROM tiangolo/uvicorn-gunicorn-fastapi:python3.7

COPY . /app/
RUN cd /app/ \
 && pip3 install -r req.txt