from oauthlib.oauth2 import BackendApplicationClient
from requests_oauthlib import OAuth2Session
import os
import time
import urllib3

backend_url = os.environ.get("BACKEND_URL")
if backend_url is None:
    raise Exception("Can't work without BACKEND_URL")

client_id = os.environ.get("CLIENT_ID")
if client_id is None:
    raise Exception("Can't work without CLIENT_ID")

client_secret = os.environ.get("CLIENT_SECRET")
if client_secret is None:
    raise Exception("Can't work without CLIENT_SECRET")

verify = os.environ.get("VERIFY_CERT")
if verify == "false":
    verify = False
    urllib3.disable_warnings()
elif verify == "true" or verify is None:
    verify = True
else:
    raise Exception("VERIFY_CERT is not true or false.")

client = BackendApplicationClient(client_id=client_id)
oauth = OAuth2Session(client=client)

_tok = None


def _get_fresh_token():
    global _tok
    if _tok is None or _tok['expires_at'] < time.time():
        token = oauth.fetch_token(
            token_url=backend_url + '/api/v1/oauth/service/token',
            client_id=client_id,
            client_secret=client_secret,
            verify=verify
        )
        token['expires_at'] = time.time() + token['expires_in'] - 60
        _tok = token
        return token
    else:
        return _tok


def get_groups():
    _get_fresh_token()
    return oauth.get(backend_url + '/api/v1/hosts-groups', verify=verify).json()


def get_hosts(page_number):
    _get_fresh_token()
    return oauth.get(backend_url + '/api/v1/hosts', verify=verify, params={
        'page': page_number,
        'sort': 'id',
    }).json()


def make_group(name, description):
    _get_fresh_token()
    return oauth.post(backend_url + '/api/v1/hosts-groups', verify=verify, json={
        "description": description,
        "name": name
    }).json()


def delete_group(group_id):
    _get_fresh_token()
    return oauth.delete(backend_url + f'/api/v1/hosts-groups/{group_id}', verify=verify).status_code == 204


def add_host_to_group(group_id, host_id):
    _get_fresh_token()
    return oauth.post(backend_url + f'/api/v1/hosts-groups/{group_id}/host', verify=verify, json={
        "hostId": host_id
    }).json()


def delete_host_from_group(group_id, host_id):
    _get_fresh_token()
    return oauth.delete(backend_url + f'/api/v1/hosts-groups/{group_id}/host', verify=verify, json={
        "hostId": host_id
    }).json()
