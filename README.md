# Automatic groups management tool

Creates and populates groups based on the  vendor/model confiuration

## Build

	docker build . -t autogrouper:1

## Setup

In the Eclypsium UI
1. Register a client the the client credentials flow on the API clients page or via a registration script.
2, Register a webhook to `http://autogrouper/hook` into backend's alerts. `autogrouper` is the name of this service, accessible from backend

Then login via SSH to the instance. In the install folder (`/opt/eclypsium/`):

For v2.4.2 and v2.5 fix the swagger file:

```bash
docker cp backend:/usr/src/chipsec/api/swagger/swagger.yaml .
docker run --rm -v "${PWD}":/workdir mikefarah/yq e -i '.paths["/api/v1/hosts-groups"].post.security += [{"serviceAuth":["fullAccess"]}]' swagger.yaml
docker run --rm -v "${PWD}":/workdir mikefarah/yq e -i '.paths["/api/v1/hosts-groups/{id}"].delete.security += [{"serviceAuth":["fullAccess"]}]' swagger.yaml
docker run --rm -v "${PWD}":/workdir mikefarah/yq e -i '.paths["/api/v1/hosts-groups/{id}/host"].post.security += [{"serviceAuth":["fullAccess"]}]' swagger.yaml
docker run --rm -v "${PWD}":/workdir mikefarah/yq e -i '.paths["/api/v1/hosts-groups/{id}/host"].delete.security += [{"serviceAuth":["fullAccess"]}]' swagger.yaml
```

Inject the new volume map into the `backend.sh`:

        sed -i -r 's|-v(.*)firmware|-v /opt/eclypsium/swagger.yaml:/usr/src/chipsec/api/swagger/swagger.yaml -v\1firmware|' backend.sh

Run it to restart the backend

        ./backend.sh

Create the autogroup env file `autogrouper.env` with `CLIENT_ID`, `CLIENT_SECRET`, `BACKEND_URL` and an optional boolean `VERIFY_CERT` (defaults to true) if backend uses TLS and invalid certificate. For example:

```bash
CLIENT_ID=<...>
CLIENT_SECRET=<...>
BACKEND_URL=https://<...>.eclypsium.dev
```

Create the autogroup runner script `autogrouper.sh`

```bash
docker ps -q --filter "name=autogrouper" | grep -q . &&
docker stop autogrouper &&
docker rm autogrouper
docker run -d --name autogrouper --env-file /opt/eclypsium/autogrouper.env --network ecnet --restart unless-stopped autogrouper:1
```

Then `chmod 700 autogrouper.sh`

## Running

Run it `./autogrouper.sh`

It will start processing new hosts. To process existing hosts run

`docker run --rm --env-file /opt/eclypsium/autogrouper.env --network ecnet autogrouper:1 python3 sync.py`
