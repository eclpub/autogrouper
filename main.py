from fastapi import FastAPI, HTTPException, Request, Query, Body
import tag_coding
import uvicorn
import be_client

app = FastAPI()


@app.get('/hello')
def print_hi(name: str):
    # Use a breakpoint in the code line below to debug your script.
    return f"Hell, {name}"


def _parse_host_data(body):
    # Skip hosts with not enough info
    if "details" not in body:
        return None, None
    if "device_data" not in body["details"] or "manufacturer" not in body["details"]["device_data"]:
        return None, None
    if "model" not in body["details"]["device_data"] and "product" not in body["details"]["device_data"]:
        return None, None

    manufacturer = body["details"]["device_data"]["manufacturer"]
    if "product" in body["details"]["device_data"]:
        product = body["details"]["device_data"]["product"]
    else:
        product = body["details"]["device_data"]["model"]

    return manufacturer, product


def _find_group_for_manufacturer_product(manufacturer, product):
    groups = be_client.get_groups()
    found_group = None
    for group in groups:
        try:
            automatic_tag = tag_coding.decode_tag(group["description"])
            if automatic_tag is None:
                continue
        except:
            continue
            pass
        if "product" in automatic_tag and automatic_tag["product"] == product and \
                "manufacturer" in automatic_tag and automatic_tag["manufacturer"] == manufacturer:
            found_group = group
            break

    return found_group


@app.post('/hook')
def hook_listener(body: dict):
    # print(json.dumps(body, indent=2))
    if "type" in body and body["type"] == "hostAction":
        if "sub_type" in body and body["sub_type"] == "scan":
            # Host scanned. Add him to group
            manufacturer, product = _parse_host_data(body)
            if manufacturer is None or product is None:
                # Skip hosts with not enough info
                return

            found_group = _find_group_for_manufacturer_product(manufacturer, product)
            if found_group is None:
                print(f"[+] Making group for {manufacturer}, {product}")

                found_group = be_client.make_group(manufacturer + " " + product, tag_coding.encode_as_tag(manufacturer, product))

            print(f"[+] Adding host {body['sender_entity_id']} to group {found_group['id']}")
            be_client.add_host_to_group(found_group["id"], body["sender_entity_id"])
            pass
        elif "sub_type" in body and body["sub_type"] == "remove":
            # We need to check for empty groups
            manufacturer, product = _parse_host_data(body)
            if manufacturer is None or product is None:
                # Skip hosts with not enough info
                return

            found_group = _find_group_for_manufacturer_product(manufacturer, product)
            if found_group and "hostsCount" in found_group and found_group["hostsCount"] == 0:
                print(f"[+] Removing group for {manufacturer}, {product}, group id = {found_group['id']}")
                be_client.delete_group(found_group["id"])

    return "Hook received"


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
